# Sujets de PSTL (Projets STL) pour l'année universitaire 2020-2021

**Responsable** : [Maryse Pelletier](mailto:Maryse.Pelletier@lip6.fr) (Maryse.Pelletier@lip6.fr)

Ce dépôt contient les descriptifs en PDF des sujets de PSTL (cf. liste ci-dessus)

**ATTENTION !** : Ce catalogue n’est pas encore complet, consultez-le régulièrement, de nouveaux sujets vont être ajoutés.

# Liste des sujets :

1. [Extraction de fragments JSON à partir
de schémas](https://gitlab.com/stl-public/pstl-2021/sujets-pstl-2021/-/blob/master/PSTL2021-BAAZIZI-Extraction-JSON.pdf)
2. [Contribution SAGE : Décomposition modulaire de 2-structures](https://gitlab.com/stl-public/pstl-2021/sujets-pstl-2021/-/blob/master/PSTL2021-Peschanski-Decomp-Modulaire.pdf)
3. [Implantation du modèle d'autoorganisation par « pulvérisation » en BCM4Java](https://gitlab.com/stl-public/pstl-2021/sujets-pstl-2021/-/blob/master/PSTL2021-Malenfant-BCM4Java.pdf)
4. [Rendre une application Web reconfigurable](https://gitlab.com/stl-public/pstl-2021/sujets-pstl-2021/-/blob/master/PSTL2021-Ziadi-Web-config.pdf)
5. [Vérifications dynamiques dans MrPython](https://gitlab.com/stl-public/pstl-2021/sujets-pstl-2021/-/blob/master/PSTL2021-Peschanski-MrPython.pdf)
6. [Localisation de features dans les applications Web](https://gitlab.com/stl-public/pstl-2021/sujets-pstl-2021/-/blob/master/PSTL2021-Ziadi-Web-features.pdf)
7. [Moteur 3D et programmation réactive](https://gitlab.com/stl-public/pstl-2021/sujets-pstl-2021/-/blob/master/PSTL2021-Peschanski-Yaw.pdf)
8. [Refactoring pour Java](https://gitlab.com/fredokun/sujets-pstl-2021/-/blob/master/PSTL2021-Thierry-Mieg-Refactor.pdf)

# Marche à suivre pour choisir un projet

- Lorsqu'un sujet vous intéresse, vous devez prendre directement contact avec son encadrant (en envoyant un mail à l’encadrant dont l'adresse est fournie dans le descriptif).

**ATTENTION** : nous n'acceptons pas les candidatures simultanées, vous devez former un binôme (éventuellement trinôme, cf. descriptif du projet) et candidater par email. En cas de réponse négative (ou d'attente prolongée de la réponse, une journée complète), vous pouvez postuler à nouveau.


  Dès qu’un encadrant vous a donné son accord pour effectuer un sujet sous sa direction, **et seulement dans ce cas**, vous devez :

  -  envoyer un email à la responsable de l'UE avec votre encadrant en copie, selon le format suivant :

Objet du message :   `[PSTL2021] - <NOM1> - <NOM2> (- <NOM 3>) : Choix Projet`

(remplacer les `<NOM1>` etc. par les noms du binôme ou trinôme)

Contenu du message :

    Sujet :  <numéro (cf. liste ci-dessus)>  <titre du sujet (dans le descriptif en PDF)>

    Encadrant : <NOM> <PRENOM>

    Etudiant 1 :  <NUM ETUDIANT>   <NOM>  <PRENOM>

    Etudiant 2 :  <NUM ETUDIANT>   <NOM>  <PRENOM>


**Rappel** :  ne faites pas ceci avant d’avoir l’accord de l’encadrant (vous risquez de vous faire éliminer du sujet après vérification).

Vous recevrez ensuite un mail de confirmation de votre inscription.

